package model;

import java.sql.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Pair;

/**
 * Database is an interface to the application database, it uses JDBC to connect
 * to a SQLite3 file.
 */
public class Database {

	/**
	 * The database connection.
	 */
	private Connection conn;

	/**
	 * Creates the database interface object. Connection to the database is
	 * performed later.
	 */
	public Database() {
		conn = null;
	}

	/**
	 * Opens a connection to the database, using the specified filename
	 */
	public boolean openConnection(String filename) {
		try {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:" + filename);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Closes the connection to the database.
	 */
	public void closeConnection() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Checks if the connection to the database has been established
	 * 
	 * @return true if the connection has been established
	 */
	public boolean isConnected() {
		return conn != null;
	}

	public ObservableList<Pallet> fetchPallets() {
		ObservableList<Pallet> pallets = FXCollections.observableArrayList();
		String query = "select * from Pallets";
		try (PreparedStatement ps = conn.prepareStatement(query)) {
			ResultSet rs = ps.executeQuery();
			Pallet pallet;
			while (rs.next()) {
				int palletId = Integer.valueOf(rs.getString("id"));
				pallet = getPalletInformation(palletId);
				pallets.add(pallet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pallets;
	}

	public Pallet getPalletInformation(int palletId) {
		Pallet pallet = new Pallet();

		String queryOne = "SELECT created_at, location, recipe_name, blocked FROM Pallets "
				+ " JOIN Recipes ON recipe_name = Recipes.name WHERE Pallets.id = ?";

		String queryTwo = "SELECT delivery_date, delivered_at, name "
				+ "FROM OrderPallets JOIN Orders ON OrderPallets.order_id = Orders.id JOIN Customers "
				+ "ON Orders.customer_id = Customers.id WHERE OrderPallets.pallet_id = ?";

		try (PreparedStatement ps = conn.prepareStatement(queryOne)) {
			ps.setInt(1, palletId);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				pallet.setId(Integer.toString(palletId));
				pallet.setLocation(rs.getString("location"));
				pallet.setCreatedAt(rs.getDate("created_at").toString());
				pallet.setType(rs.getString("recipe_name"));
				int isBlocked = rs.getInt("blocked");
				if (isBlocked == 1) {
					pallet.setBlocked("yes is blocked");
				} else {
					pallet.setBlocked("no");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try (PreparedStatement ps = conn.prepareStatement(queryTwo)) {
			ps.setInt(1, palletId);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				pallet.setDeliveryDate(rs.getDate("delivery_date").toString());
				Date deliveredAt = rs.getDate("delivered_at");
				if (deliveredAt != null) {
					pallet.setDeliveredAt(deliveredAt.toString());
				} else {
					pallet.setDeliveredAt("Not delivered");
				}
				pallet.setCustomerName(rs.getString("name"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return pallet;
	}

	public boolean createNewPallet(String cookieType) {
		String query = "SELECT material_type, quantity FROM RecipeItems WHERE recipe_name = ?";
		ArrayList<Pair<String, Integer>> ingredients = new ArrayList<Pair<String, Integer>>();

		try (PreparedStatement ps = conn.prepareStatement(query)) {
			ps.setString(1, cookieType);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				ingredients.add(new Pair<String, Integer>(rs.getString("material_type"), rs.getInt("quantity")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (ingredients.isEmpty()) {
			return false;
		}
		String queryTwo = "SELECT material_type FROM RawMaterials WHERE material_type = ? AND total_quantity > ?";

		for (int i = 0; i < ingredients.size(); i++) {
			try (PreparedStatement ps = conn.prepareStatement(queryTwo)) {
				ps.setString(1, ingredients.get(i).getKey());
				ps.setInt(2, ingredients.get(i).getValue() * 10 * 36);

				ResultSet rs = ps.executeQuery();
				if (!rs.next()) {
					return false;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		// här vet vi att vi kan skapa en pall med kakor, det finns ingredienser

		String queryThree = "UPDATE RawMaterials SET total_quantity = total_quantity - ? WHERE material_type = ?";

		for (int i = 0; i < ingredients.size(); i++) {
			try (PreparedStatement ps = conn.prepareStatement(queryThree)) {
				ps.setInt(1, ingredients.get(i).getValue() * 10 * 36);
				ps.setString(2, ingredients.get(i).getKey());
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		String queryFour = "INSERT INTO Pallets(" + "recipe_name," + "created_at," + "location," + "blocked)"
				+ "VALUES(?,?,?,0)";

		try (PreparedStatement ps = conn.prepareStatement(queryFour)) {
			ps.setString(1, cookieType);
			ps.setDate(2, Date.valueOf(LocalDate.now()));
			ps.setString(3, "storage");
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return true;
	}

	public ObservableList<Pallet> getPalletsWithType(String cookieType) {
		ObservableList<Pallet> pallets = FXCollections.observableArrayList();
		String query = "SELECT id " + "FROM Pallets " + "JOIN Recipes ON recipe_name = Recipes.name "
				+ "WHERE recipe_name = ?";
		try (PreparedStatement ps = conn.prepareStatement(query)) {
			ps.setString(1, cookieType);
			ResultSet rs = ps.executeQuery();
			Pallet pallet;
			while (rs.next()) {
				int palletId = Integer.valueOf(rs.getString("id"));
				pallet = getPalletInformation(palletId);
				pallets.add(pallet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pallets;
	}

	public ObservableList<Pallet> getPalletsBetweenDates(Date madeAfter, Date madeBefore) {
		ObservableList<Pallet> pallets = FXCollections.observableArrayList();

		String query = "SELECT id " + "FROM Pallets " + "WHERE created_at > ? AND created_at < ?";

		try (PreparedStatement ps = conn.prepareStatement(query)) {
			ps.setDate(1, madeAfter);
			ps.setDate(2, madeBefore);
			ResultSet rs = ps.executeQuery();
			Pallet pallet;
			while (rs.next()) {
				int palletId = Integer.valueOf(rs.getString("id"));
				pallet = getPalletInformation(palletId);
				pallets.add(pallet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pallets;
	}

	public ObservableList<Pallet> getPalletsToCustomer(String customerName) {
		ObservableList<Pallet> pallets = FXCollections.observableArrayList();
		String query = "SELECT pallet_id " + "FROM Customers " + "JOIN Orders ON Customers.id = Orders.customer_id "
				+ "JOIN OrderPallets ON Orders.id = OrderPallets.order_id "
				+ "WHERE Customers.name = ? AND Orders.delivered_at > 0";

		try (PreparedStatement ps = conn.prepareStatement(query)) {
			ps.setString(1, customerName);
			ResultSet rs = ps.executeQuery();
			Pallet pallet;
			while (rs.next()) {
				int palletId = Integer.valueOf(rs.getString("pallet_id"));
				pallet = getPalletInformation(palletId);
				pallets.add(pallet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pallets;
	}

	public ObservableList<Pallet> getBlockedPallets() {
		ObservableList<Pallet> pallets = FXCollections.observableArrayList();
		String query = "SELECT id " + "FROM Pallets " + "WHERE blocked = 1";

		try (PreparedStatement ps = conn.prepareStatement(query)) {
			ResultSet rs = ps.executeQuery();
			Pallet pallet;
			while (rs.next()) {
				int palletId = Integer.valueOf(rs.getString("id"));
				pallet = getPalletInformation(palletId);
				pallets.add(pallet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pallets;
	}

	public boolean blockPalletsWithCookieType(String cookieType, Date madeAfter, Date madeBefore) {
		String query = "UPDATE Pallets " + "SET blocked = 1 "
				+ "WHERE recipe_name = ? AND created_at > ? AND created_at < ?";

		try (PreparedStatement ps = conn.prepareStatement(query)) {
			ps.setString(1, cookieType);
			ps.setDate(2, madeAfter);
			ps.setDate(3, madeBefore);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public ObservableList<String> getRecipes() {
		ObservableList<String> recipes = FXCollections.observableArrayList();
		String query = "SELECT name " + "FROM Recipes";

		try (PreparedStatement ps = conn.prepareStatement(query)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				recipes.add(rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return recipes;
	}

}
