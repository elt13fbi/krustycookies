CREATE TABLE IF NOT EXISTS RawMaterials(
	material_type			TEXT PRIMARY KEY NOT NULL,
	total_quantity			INT,
	last_delivery_date		DATE,
	last_delivery_amount	INT
);

CREATE TABLE IF NOT EXISTS RecipeItems(
	id						INTEGER PRIMARY KEY,
	material_type				INTEGER NOT NULL,
	recipe_name				TEXT NOT NULL,
	quantity				INTEGER NOT NULL,
	FOREIGN KEY(material_type)	REFERENCES RawMaterials(material_type),
	FOREIGN KEY(recipe_name)		REFERENCES Recipes(name)	
);

CREATE TABLE IF NOT EXISTS Recipes(
	name					TEXT PRIMARY KEY NOT NULL
);

CREATE TABLE IF NOT EXISTS OrderItems(
	id						INTEGER PRIMARY KEY,
	recipe_name				TEXT NOT NULL,
	order_id				INTEGER NOT NULL,
	quanitity				INTEGER NOT NULL,
	FOREIGN KEY(recipe_name)		REFERENCES Recipes(name),
	FOREIGN KEY(order_id)		REFERENCES Orders(id) 
);

CREATE TABLE IF NOT EXISTS Orders(
	id						INTEGER PRIMARY KEY,
	delivery_date			DATE NOT NULL,
	delivered_at			DATE,
	customer_id				INTEGER NOT NULL,
	loading_bill_id			INTEGER,
	FOREIGN KEY(customer_id)		REFERENCES Customers(id),
	FOREIGN KEY(loading_bill_id)	REFERENCES LoadingBills(id)
);

CREATE TABLE IF NOT EXISTS Customers(
	id						INTEGER PRIMARY KEY,
	name					TEXT NOT NULL,
	address					TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS OrderPallets(
	id						INTEGER PRIMARY KEY,
	order_id				INTEGER NOT NULL,
	pallet_id				INTEGER NOT NULL,
	FOREIGN KEY(order_id)		REFERENCES Orders(id),
	FOREIGN KEY(pallet_id)		REFERENCES Pallets(id)
);

CREATE TABLE IF NOT EXISTS Pallets(
	id						INTEGER PRIMARY KEY,
	recipe_name				TEXT NOT NULL,
	created_at				DATE NOT NULL,
	location				TEXT NOT NULL,
	blocked					INTEGER NOT NULL,
	FOREIGN KEY(recipe_name)		REFERENCES Recipes(name)
);

